//
//  AddCategoryViewController.swift
//  RexFit
//
//  Created by Никита on 10.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class AddCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var addCategoryTable: UITableView!
    
    func cancel(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func done(sender: UIBarButtonItem) {
        let indexPath = IndexPath(row: 1, section: 0)
        let cell = addCategoryTable.cellForRow(at: indexPath) as! NameTableViewCell
        
        guard let text = cell.textField.text, !text.isEmpty else {
            showAlert(title: "Error", message: "Name field must be filled.")
            return
        }
        
        defaultCategoriesArray.append(text)
        defaults.set(defaultCategoriesArray, forKey: "defaultCategoriesArray")
        defaults.synchronize()
        
        moreCategories.append(text)
        moreExerciseData.updateValue([], forKey: text)
        dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addCategoryTable.dataSource = self
        addCategoryTable.delegate = self
        
         navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addLeftButton()
        addRightButton()
    }
    
    func addLeftButton() {
        let buttonCancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action:  #selector(cancel(sender:)))
        self.navigationItem.leftBarButtonItem = buttonCancel
    }
    
    func addRightButton(){
        let buttonDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action:  #selector(done(sender:)))
        self.navigationItem.rightBarButtonItem = buttonDone
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameAddcategoryCellID")
                cell?.selectionStyle = .none
                return cell!
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "textAddCategoryICellD") as! NameTableViewCell
                cell.selectionStyle = .none
                return cell
            default: break
        }
        
        return UITableViewCell()
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        
//        switch indexPath.row {
//        case 1:
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MoreExercisesViewControllerID") as! MoreExerciseViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//        case 2:
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MoreCategoriesViewControllerID") as! MoreCategoriesViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//        default: break
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
