//
//  EditExerciseViewController.swift
//  RexFit
//
//  Created by Никита on 14.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class EditExerciseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var editExerciseTable: UITableView!

    func back(sender: UIBarButtonItem) {
        if !(selectedExercise is DefaultExercise) {
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = editExerciseTable.cellForRow(at: indexPath) as! EditExerciseTableViewCell
            guard let text = cell.textField.text, !text.isEmpty else {
                showAlert(title: "Error", message: "Name field must be filled.")
                return
            }
        
            let indexPath1 = IndexPath(row: 1, section: 0)
            let cell1 = editExerciseTable.cellForRow(at: indexPath1)
            guard let category = cell1?.detailTextLabel?.text, !category.isEmpty else {
                showAlert(title: "Error", message: "Category field must be filled.")
                return
            }
            let newExercise = Exercise(name: text, category: category, type: .strength)
            let index = moreExerciseData[category]?.index(of: selectedExercise)
            moreExerciseData[category]?[index!] = newExercise
            let exerciseDict = ["name": text, "category": category, "type": "strength"] // change
            let oldExerciseDict = ["name": selectedExercise.name, "category": selectedExercise.category, "type": "strength"]
            var indexDefault = 0
            var checker = false
            for element in defaultExerciseArray {
                indexDefault = indexDefault + 1
                if element == oldExerciseDict {
                    checker = true
                    indexDefault = indexDefault - 1
                    break
                }}
            if checker {
            defaultExerciseArray.remove(at: indexDefault)
            }
            defaultExerciseArray.append(exerciseDict as [String : String])
            defaults.set(defaultExerciseArray, forKey: "defaultExerciseArray")
            defaults.synchronize()
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    var selectedExercise: Exercise!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editExerciseTable.dataSource = self
        editExerciseTable.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addLeftButton()
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseNameCell") as? EditExerciseTableViewCell
            cell?.textField?.text = selectedExercise.name
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseTypeCell")
            cell?.detailTextLabel?.text = selectedExercise.category
            cell?.textLabel?.text = "Category"
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseCategoryCell")
            cell?.detailTextLabel?.text = String(describing: selectedExercise.type)
            cell?.textLabel?.text = "Type"
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseSpaceCell")
            self.editExerciseTable.rowHeight = 25
            return cell!
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "deleteExerciseCell")
            self.editExerciseTable.rowHeight = 44
            return cell!
        default: break
        }
        
        return UITableViewCell()
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 3 {
//            return 117
//        } else {
//            return 44
//        }
//    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 4 {
            if !(selectedExercise is DefaultExercise) {
            let exerciseDict = ["name": selectedExercise.name,"category": selectedExercise.category, "type": String(describing: selectedExercise.type)]
            guard let indexID = moreExerciseData[selectedExercise.category]?.index(of: selectedExercise) else {
                return
            }
            var indexDefault = 0
            var checker = false
            for element in defaultExerciseArray {
                indexDefault = indexDefault + 1
                if element == exerciseDict {
                    checker = true
                    indexDefault = indexDefault - 1
                    break
                }
            }
            print(indexDefault)
            moreExerciseData[selectedExercise.category]?.remove(at: indexID)
            if checker {
                defaultExerciseArray.remove(at: indexDefault)
                }
            defaults.set(defaultExerciseArray, forKey: "defaultExerciseArray")
            defaults.synchronize()
            navigationController?.popViewController(animated: true)
        } else {
                showAlert(title: "Error", message: "Standart exercise couldnt be deleted.")
            }
        }
    }

    func addLeftButton(){
        let buttonBack = UIBarButtonItem(title: "Back", style: .plain, target: self, action:  #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = buttonBack
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
