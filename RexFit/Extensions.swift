//
//  Extensions.swift
//  RexFit
//
//  Created by Никита on 16.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

extension String {
    func convertToEnum() -> ExerciseType {
        switch self.lowercased() {
        case "strength":
            return .strength
        case "cardio":
            return .cardio
        case "other":
            return .other
        default:
            return .strength
        }
    }
}
