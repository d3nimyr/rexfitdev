//
//  ChooseCategoryViewController.swift
//  RexFit
//
//  Created by Никита on 12.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class ChooseCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func cancel(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    
    @IBOutlet weak var chooseCategoryTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        chooseCategoryTable.dataSource = self
        chooseCategoryTable.delegate = self
        addLeftButton()
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
    }
    
    func addLeftButton() {
        let buttonCancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action:  #selector(cancel(sender:)))
        self.navigationItem.leftBarButtonItem = buttonCancel
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chooseCategoryCellID")
        cell?.textLabel?.text = moreCategories[indexPath.row]
        return cell!
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moreCategories.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chosenCategory = moreCategories[indexPath.row]
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
