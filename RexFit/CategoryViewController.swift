//
//  ViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

let defaults = UserDefaults.standard


public class Exercise {
    var name: String
    var category: String
    var type: ExerciseType
    
    init(name: String, category: String, type: ExerciseType) {
        self.name = name
        self.type = type
        self.category = category
    }
}

enum ExerciseType {
    case strength
    case cardio
    case other
}

extension Exercise: Equatable {
    public static func == (lhs: Exercise, rhs: Exercise) -> Bool {
        return
                lhs.name == rhs.name &&
                lhs.type == rhs.type &&
                lhs.category == rhs.category
    }
}

public class DefaultExercise: Exercise {
    var description: String
    var image: UIImage
    
    init(description: String, image: UIImage, name: String, category: String, type: ExerciseType) {
        self.description = description
        self.image = image
        super.init(name: name, category: category, type: type)
    }
}

public var categories = [String](exerciseData.keys)

class CategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var filteredData = [String]()
    var isSearching = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.dataSource = self
        table.delegate = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
        tabBarController?.tabBar.tintColor = UIColor(netHex: 0x08b6c6)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "catCell")
            cell?.textLabel?.text = filteredData[indexPath.row]
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "catCell")
            cell?.textLabel?.text = categories[indexPath.row]
            return cell!
        }
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        } else {
            return categories.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "exerciseID") as! ExersicesViewController
        if isSearching {
            vc.selectedCategory = filteredData[indexPath.row]
        } else {
        vc.selectedCategory = categories[indexPath.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            table.reloadData()
        } else {
            isSearching = true
            filteredData = searchText.isEmpty ? categories : categories.filter({(dataString: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
            table.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
