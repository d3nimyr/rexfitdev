//
//  SingleExerciseViewController.swift
//  RexFit
//
//  Created by d3nimyr on 10/07/17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class SingleExerciseViewController: UIViewController {
    
    @IBOutlet weak var singleExerciseText: UITextView!
    @IBOutlet weak var singleExerciseImageView: UIImageView!
    
    var selectedexercise:DefaultExercise!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        title = selectedexercise.name
        singleExerciseText.text = selectedexercise.description
        singleExerciseImageView.image = selectedexercise.image
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
