//
//  MoreCategoriesViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class MoreCategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var moreCategoriesTable: UITableView!
    
    var filteredData = [String]()
    
    var isSearching = false
    
    func addTapped(sender: UIBarButtonItem){
       presentWithNC(withIdentifier: "addCategoryViewControllerID")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Categories"
        
        moreCategoriesTable.dataSource = self
        moreCategoriesTable.delegate = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addLeftButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.moreCategoriesTable.reloadData()
    }
    
    func addLeftButton() {
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped(sender:)))
        navigationItem.rightBarButtonItem = add
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCategoriesCell")
            cell?.textLabel?.text = filteredData[indexPath.row]
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCategoriesCell")
        cell?.textLabel?.text = moreCategories[indexPath.row]
            return cell!
        }
        
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        } else {
        return moreCategories.count
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editCategorieViewControllerID") as! EditCategorieViewController
        vc.selectedCategory = moreCategories[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
      }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            moreCategoriesTable.reloadData()
        } else {
            isSearching = true
            filteredData = searchText.isEmpty ? moreCategories : moreCategories.filter({(dataString: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
            moreCategoriesTable.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
