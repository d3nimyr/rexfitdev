//
//  Sinder.swift
//  FamilyApp
//
//  Created by Add Creative Inc. on 09.06.16.
//  Copyright © 2016 Add Creative Inc. All rights reserved.
//

import UIKit
import MapKit
public extension CLLocationCoordinate2D {
    func toLocation() -> CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
}

public extension UINavigationController {
    func hide() {
        navigationBar.isHidden = true
    }
    
    func show() {
        navigationBar.isHidden = false
    }
}


public extension UIView {
    
    public var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    public var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don"t touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    
    public func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                          shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                          shadowOpacity: Float = 0.4,
                          shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}

extension CLPlacemark {
    func toAddress() -> String {
        var address = ""
        if let country = subAdministrativeArea {
            address += country
            address += ", "
        }
        
        if let city = locality {
            address += city
            address += ", "
        }
        
        if let street = thoroughfare {
            address += street
            address += ", "
        }
        
        if let number = subThoroughfare {
            address += number
            address += ", "
        }
        
        return address
    }
    
}

public enum transferMethod {
    case push
    case pop
    case present
    case show
}

public enum Devicer {
    case iPadPro
    case iPad
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhone6Plus
    case undefiened
}

public extension UITableView {
    func indexPathForView(view: AnyObject) -> IndexPath? {
        let originInTableView = self.convert(CGPoint.zero, from: (view as! UIView))
        return self.indexPathForRow(at: originInTableView)
    }
}

public extension Date {
    
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    
    func convertFormateToNormDateString(format: String) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        let timeStamp = dateFormatter.string(from: self)
        
        return timeStamp
    }
    
    var startOfDay: NSDate {
        return NSCalendar.current.startOfDay(for: self) as NSDate
    }
    
    var endOfDay: NSDate? {
        let components = NSDateComponents()
        components.day = 1
        components.second = -1
        return NSCalendar.current.date(byAdding: components as DateComponents, to: startOfDay as Date)! as NSDate
    }
}

extension String {
    func removeText(text: String) -> String {
        return replacingOccurrences(of: text, with: "")
    }
    
    func toLatinTransliteration() -> String {
        var returned = ""
        let rus = [" ","а","б","в","г","д","е","ё", "ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х", "ц","ч", "ш","щ","ъ","ы","ь","э", "ю","я","А","Б","В","Г","Д","Е","Ё", "Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х", "Ц", "Ч","Ш", "Щ","Ъ","Ы","Б","Э","Ю","Я","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y", "Z"]
        let eng = [" ","a","b","v","g","d","e","e","zh","z","i","y","k","l","m","n","o","p","r","s","t","u","f","h","ts","ch","sh","sch", "","i", "","e","ju","ja","A","B","V","G","D","E","E","Zh","Z","I","Y","K","L","M","N","O","P","R","S","T","U","F","H","Ts","Ch","Sh","Sch", "","I", "","E","Ju","Ja","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
        
        for character in characters {
            for r in rus where String(describing: character) == r {
                let index = rus.index(of: r)
                returned += eng[index!]
            }
        }
        
        return returned
    }
    
    
}

public extension UIViewController {
    
    func calibriForNavigationBar() {
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Calibri", size: 18.0)!]
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    func locationWithBearing(bearing:Double, distanceMeters:Double, origin:CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let distRadians = distanceMeters / (6372797.6) // earth radius in meters
        
        let lat1 = origin.latitude * Double.pi / 180
        let lon1 = origin.longitude * Double.pi / 180
        
        let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(bearing))
        let lon2 = lon1 + atan2(sin(bearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))
        
        return CLLocationCoordinate2D(latitude: lat2 * 180 / Double.pi, longitude: lon2 * 180 / Double.pi)
    }
    
    
    func getDevice() -> Devicer {
        let frame = self.view.frame.size
        
        if frame == CGSize(width: 1024, height: 1366) {
            return Devicer.iPadPro
        } else if frame == CGSize(width: 768, height: 1024) {
            return Devicer.iPad
        } else if frame == CGSize(width: 320, height: 480) {
            return Devicer.iPhone4
        } else if frame == CGSize(width: 320, height: 568) {
            return Devicer.iPhone5
        } else if frame == CGSize(width: 375, height: 667) {
            return Devicer.iPhone6
        } else if frame == CGSize(width: 414, height: 736) {
            return Devicer.iPhone6Plus
        } else {
            return Devicer.undefiened
        }
    }
    
    func smartBack() {
        if isModal() {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func showAlertWithOneAction(title: String, message: String, handle: @escaping () -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                handle()
        }
        alertController.addAction(okAction)
    
        self.present(alertController, animated: true, completion: nil)

    }
    
    func isModal() -> Bool {
        
        if let navigationController = self.navigationController{
            if navigationController.viewControllers.first != self{
                return false
            }
        }
        
        if self.presentingViewController != nil {
            return true
        }
        
        if self.presentingViewController?.presentedViewController == self {
            return true
        }
        
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }
        
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }

    
    //set image to View and add new elemnt UIImageView
    func setImage(image: UIImage, toView: UIView) {
        let replaceView = UIImageView(image: image)
        replaceView.frame = CGRect(x: 0,y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(replaceView)
        self.view.sendSubview(toBack: replaceView)
    }
    
    // open new VC
    func transferVCwith(identifier: String, method: transferMethod) {
        if method == .pop {
            _ = navigationController?.popViewController(animated: true)
        } else if method == .present {
            self.present(self.storyboard!.instantiateViewController(withIdentifier: identifier), animated: true, completion: nil)
        } else if method == .push {
            self.navigationController?.pushViewController(self.storyboard!.instantiateViewController(withIdentifier: identifier), animated: true)
        } else if method == .show {
            self.show(self.storyboard!.instantiateViewController(withIdentifier: identifier), sender: self)
        }
    }
    
    func presentWithNC(withIdentifier identifier: String) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: identifier) else {
            return
        }
        
        present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    //easy way to show alert
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithActions(title: String, message: String, withCancelButton: Bool, buttons: [(title: String, action: () -> Void)]) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for button in buttons {
            let okAction = UIAlertAction(title: button.title, style: UIAlertActionStyle.default) {
                UIAlertAction in
                button.action()
            }
            alertController.addAction(okAction)
        }
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .default, handler: nil)
        if withCancelButton {
            alertController.addAction(cancelAction)
        }
    
        self.present(alertController, animated: true, completion: nil)
    }
    
    //checking email string is valid
    func isValid(email : String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if emailTest.evaluate(with: email) {
            return true
        }
        return false
    }
}

public extension NSObject {
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func ID() -> String {
        let time = String(Int(NSDate().timeIntervalSince1970), radix: 16, uppercase: false)
        let machine = String(arc4random_uniform(900000) + 100000)
        let pid = String(arc4random_uniform(9000) + 1000)
        let counter = String(arc4random_uniform(900000) + 100000)
        return time + machine + pid + counter
    }
    
    //delay in seconds with closure
    func delay(delay:Double, closure:@escaping ()->()) {
        
        let when = DispatchTime.now() + delay // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure()
        }
    }
}

//set image to imageView from string
extension UIImageView {
    convenience init(string: String) {
        self.init(image: UIImage(named: string))
    }
}

extension UIColor {
    //UIColor from HEX
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

