//
//  EditCategorieViewController.swift
//  RexFit
//
//  Created by Никита on 14.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class EditCategorieViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var editCategoryTable: UITableView!

    var selectedCategory: String!
    var exercises = [Exercise]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editCategoryTable.dataSource = self
        editCategoryTable.delegate = self
        editCategoryTable.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addLeftButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        exercises = moreExerciseData[selectedCategory]!
        self.editCategoryTable.reloadData()
    }
    
    func back(sender: UIBarButtonItem) {
        if categories.contains(selectedCategory) {
            navigationController?.popViewController(animated: true)
        } else {
            let indexPath = IndexPath(row: 1, section: 0)
            let cell = editCategoryTable.cellForRow(at: indexPath) as! EditCategoryNameTableViewCell
            guard let text = cell.nameText.text, !text.isEmpty else {
                showAlert(title: "Error", message: "Name field must be filled.")
                return
            }
            let index = defaultCategoriesArray.index(of: selectedCategory)
            let index1 = moreCategories.index(of: selectedCategory)
            defaultCategoriesArray[index!] = text
            defaults.set(defaultCategoriesArray, forKey: "defaultCategoriesArray")
            defaults.synchronize()
        
            moreCategories[index1!] = text
            moreExerciseData.updateValue([], forKey: text)
        
            navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editCategoryNameCellID")
            cell?.separatorInset = .zero
            return cell!
        } else
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editTextNameCellID") as? EditCategoryNameTableViewCell
            cell?.nameText?.text = selectedCategory
            cell?.separatorInset = .zero
            return cell!
        } else
            if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseNameCellID")
                cell?.separatorInset = .zero
                tableView.reloadRows(at: <#T##[IndexPath]#>, with: .none)
                return cell!
        } else
            if indexPath.row >= 3 && indexPath.row < 3 + exercises.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseNameCellID")
                cell?.textLabel?.text = exercises[indexPath.row - 3].name
                cell?.separatorInset = .zero
                cell?.backgroundColor = UIColor.white
                return cell!
        } else
            if indexPath.row == 3 + exercises.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "editCategorySpaceCellID")
                cell?.separatorInset = .zero
                self.editCategoryTable.rowHeight = 25
                return cell!
        } else
            if indexPath.row == 4 + exercises.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "deleteCategoryCellID")
                cell?.separatorInset = .zero
                self.editCategoryTable.rowHeight = 44
                return cell!
        }
        return UITableViewCell()
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exercises.count + 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= 3 && indexPath.row < 3 + exercises.count {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "editExerciseViewControllerID") as! EditExerciseViewController
            vc.selectedExercise = exercises[indexPath.row - 3]
            self.navigationController?.pushViewController(vc, animated: true)
        } else
            if indexPath.row == 4 && exercises.count == 0 {
               let defaultIndex = defaultCategoriesArray.index(of: selectedCategory)
               let categoryIndex = moreCategories.index(of: selectedCategory)
               defaultCategoriesArray.remove(at: defaultIndex!)
               defaults.set(defaultCategoriesArray, forKey: "defaultCategoriesArray")
               defaults.synchronize()
               moreCategories.remove(at: categoryIndex!)
               moreExerciseData.removeValue(forKey: selectedCategory)
               navigationController?.popViewController(animated: true)
        } else
                if indexPath.row == 4 + exercises.count {
                    showAlert(title: "Error", message: "Category contains exercises.")
        }
        
    }
    
    func addLeftButton(){
        let buttonBack = UIBarButtonItem(title: "Back", style: .plain, target: self, action:  #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = buttonBack
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
