//
//  TypePickerTableViewCell.swift
//  RexFit
//
//  Created by Никита on 13.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class TypePickerTableViewCell: UITableViewCell {

    @IBOutlet weak var typePicker: UIPickerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
