//
//  AddExerciseViewController.swift
//  RexFit
//
//  Created by Никита on 11.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
public var chosenCategory: String = ""

class AddExerciseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var addExerciseTable: UITableView!
    fileprivate let types = ["Strength", "Cardio", "Other"]
    fileprivate var selectedType = "strength"
    
    var pickerStatus = 0 {
        didSet {
            let indexPath = IndexPath(row: 3, section: 0)
            if pickerStatus == 1 {
                addExerciseTable.insertRows(at: [indexPath], with: .top)
            } else {
                addExerciseTable.deleteRows(at: [indexPath], with: .top)
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedType = types[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return types[row]
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func cancel(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func done1(sender: UIBarButtonItem) {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = addExerciseTable.cellForRow(at: indexPath) as! NameExerciseTableViewCell
        guard let text = cell.nameTextField.text, !text.isEmpty else {
            showAlert(title: "Error", message: "Name field must be filled.")
            return
        }
        
        let indexPath1 = IndexPath(row: 1, section: 0)
        let cell1 = addExerciseTable.cellForRow(at: indexPath1)
        guard let category = cell1?.detailTextLabel?.text, !category.isEmpty else {
            showAlert(title: "Error", message: "Category field must be filled.")
            return
        }
        let newExercise = Exercise(name: text, category: category, type: .strength)
        moreExerciseData[category]?.append(newExercise)
        print(moreExerciseData)
        let exerciseDict = ["name": text,"category": category, "type": selectedType.lowercased()]
        defaultExerciseArray.append(exerciseDict as [String : String])
        defaults.set(defaultExerciseArray, forKey: "defaultExerciseArray")
        defaults.synchronize()
        dismiss(animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addExerciseTable.dataSource = self
        addExerciseTable.delegate = self
        addExerciseTable.separatorInset = UIEdgeInsetsMake(0, 15000, 0, 0)
        
        title = "Add Exercise"
        
         navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addLeftButton()
        addRightButton()
    }

        func addLeftButton() {
        let buttonCancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action:  #selector(cancel(sender:)))
        self.navigationItem.leftBarButtonItem = buttonCancel
    }
    
    func addRightButton(){
        let buttonDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action:  #selector(done1(sender:)))
        self.navigationItem.rightBarButtonItem = buttonDone
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addExerciseTable.reloadData()
    }
    
    func done(sender: UIBarButtonItem) {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = addExerciseTable.cellForRow(at: indexPath) as! NameExerciseTableViewCell
        
        guard let text = cell.nameTextField.text, !text.isEmpty else {
            
            return
        }

    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameExerciseTableViewCellID") as! NameExerciseTableViewCell?
            cell?.separatorInset = UIEdgeInsets.zero
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameExerciseTableViewCellID2")
            cell?.selectionStyle = .none
            cell?.detailTextLabel?.text = chosenCategory
            cell?.separatorInset = UIEdgeInsets.zero
            cell?.detailTextLabel?.textColor = UIColor.lightGray
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameExerciseTableViewCellID3")
            cell?.selectionStyle = .none
            cell?.detailTextLabel?.textColor = UIColor.lightGray
            cell?.detailTextLabel?.text = selectedType
            cell?.separatorInset = UIEdgeInsets.zero
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "yypePickerTableViewCellID") as! TypePickerTableViewCell
            cell.typePicker.dataSource = self
            cell.separatorInset = UIEdgeInsets.zero
            cell.typePicker.delegate = self
            return cell
        default: break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 {
            return 117
        } else {
            return 44
        }
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3 + pickerStatus
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 1 {
            presentWithNC(withIdentifier: "chooseCategoryViewControllerID")
        }
        else if indexPath.row == 2{
            pickerStatus = pickerStatus == 0 ? 1 : 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
