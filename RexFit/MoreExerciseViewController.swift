//
//  MoreExerciseViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

public var mappedValues: [Exercise] = []

class MoreExerciseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var moreExercisesViewControllerTable: UITableView!

    
    var mappedDefaultExerciese: [DefaultExercise] =  []
    func addTapped(sender: UIBarButtonItem){
        presentWithNC(withIdentifier: "addExerciseViewControllerID")
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moreExercisesViewControllerTable.dataSource = self
        moreExercisesViewControllerTable.delegate = self
        
        title = "Exercises"
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addRightButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mappedValues = moreExerciseData.map { $0.value }.flatMap{$0} // Объединяем массивы в один
        self.moreExercisesViewControllerTable.reloadData()
    }
    
    func addRightButton() {
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped(sender:)))
        navigationItem.rightBarButtonItem = add
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreExercisesViewControllerCell")
        cell?.textLabel?.text = mappedValues[indexPath.row].name
        
        return cell!
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mappedValues.count
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editExerciseViewControllerID") as! EditExerciseViewController
        vc.selectedExercise = mappedValues[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
      }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
