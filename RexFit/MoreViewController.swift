//
//  MoreViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit


class MoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var moreTable: UITableView! // moreTableView переименуй
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegating and datasourcing
        moreTable.dataSource = self
        moreTable.delegate = self
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
        
        moreCategories = [String](moreExerciseData.keys)
        moreTable.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0) // disable separator
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell")
                cell?.separatorInset = UIEdgeInsets.zero
                cell?.selectionStyle = .none
                return cell!
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell")
                cell?.separatorInset = UIEdgeInsets.zero
                cell?.textLabel?.text = "Exercise"
                return cell!
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell")
                cell?.separatorInset = UIEdgeInsets.zero
                cell?.textLabel?.text = "Categories"
                return cell!
            default: break
        }
        
        return UITableViewCell()
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MoreExercisesViewControllerID") as! MoreExerciseViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MoreCategoriesViewControllerID") as! MoreCategoriesViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default: break
        }
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
