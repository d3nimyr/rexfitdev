//
//  ExersicesViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class ExersicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate  {
    
    @IBOutlet weak var exetciseTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var filteredData = [String]()
    var isSearching = false
    var selectedCategory: String!
    var exersices = [DefaultExercise]()
    var exerciseNames = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exetciseTable.dataSource = self
        exetciseTable.delegate = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        exersices = exerciseData[selectedCategory]!
        exerciseNames = exersices.map({ (exercise) -> String in
            return exercise.name
        })
        title = "Exercises" // можно просто тайтл
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x08b6c6)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if isSearching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "catCell")
            cell?.textLabel?.text = filteredData[indexPath.row]
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell")
            cell?.textLabel?.text = exerciseNames[indexPath.row]
            return cell!
        }
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        } else {
            return exerciseNames.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "singleExerciseViewControllerID") as! SingleExerciseViewController
        vc.selectedexercise = exersices[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            exetciseTable.reloadData()
        } else {
            isSearching = true
            filteredData = searchText.isEmpty ? exerciseNames : exerciseNames.filter({(dataString: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
            exetciseTable.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
