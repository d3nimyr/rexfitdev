//
//  AppDelegate.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import Firebase

public var moreExerciseData: [String : [Exercise]] = [:]
public var defaultCategoriesArray: [String] = (defaults.array(forKey: "defaultCategoriesArray") as? [String]) ?? []
public var defaultExerciseArray = defaults.array(forKey: "defaultExerciseArray") as? [[String: String]] ?? []
public var moreCategories = [String]()
public var exerciseData: [String: [DefaultExercise]] = [
    "Abs" :
        [
            DefaultExercise(description: "example", image: UIImage(), name: "1", category: "Abs", type: .strength),
            DefaultExercise(description: "example", image: UIImage(), name: "u", category: "Abs", type: .strength)
    ],
    "Back" :
        [
            DefaultExercise(description: "check", image: UIImage(), name: "try", category: "Back", type: .cardio)
    ],
    "Biceps" :
        [
            DefaultExercise(description: "d'lv,", image: UIImage(), name: "d,cd',", category: "Biceps", type: .other)
    ]
]


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func loadExerciseData() {
        moreExerciseData = exerciseData
        for key in defaultCategoriesArray{
            if moreExerciseData[key] == nil {
            moreExerciseData.updateValue([], forKey: key)
            }
        }
        for exercise in defaultExerciseArray {
            moreExerciseData[exercise["category"]!]?.append(Exercise(name: exercise["name"]!, category: exercise["category"]!, type: exercise["type"]!.convertToEnum()))
        }
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        loadExerciseData()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

